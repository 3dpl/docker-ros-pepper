#!/bin/bash

if [[ $1 == "" ]] ; then
    echo "./run-client idcontainer"
    echo "Introducir el identificador del contenedor."
    exit 1
fi

NAME_CONTAINER=$1

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

nvidia-docker run -it \
        --volume=$XSOCK:$XSOCK:rw \
        --volume=$XAUTH:$XAUTH:rw \
        --env="XAUTHORITY=${XAUTH}" \
        --env="DISPLAY" \
	--env="QT_X11_NO_MITSHM=1" \
        --user="userros" \
    $NAME_CONTAINER \
    bash -c "roscore & rosrun rviz rviz"
