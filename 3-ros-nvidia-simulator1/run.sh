#!/bin/bash

#NAME_CONTAINER=$1
NAME_CONTAINER="simulator"

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

echo "Iniciando..."
nvidia-docker stop simulator1 2>/dev/null 1/dev/null
nvidia-docker rm simulator1 2>/dev/null 1>/dev/null
nvidia-docker run -d -it \
        --volume=$XSOCK:$XSOCK:rw \
        --volume=$XAUTH:$XAUTH:rw \
        --env="XAUTHORITY=${XAUTH}" \
        --env="DISPLAY" \
        --env="QT_X11_NO_MITSHM=1" \
        --user="pepper" \
    --name=simulator1 $NAME_CONTAINER

#nvidia-docker exec -it simulator1 /bin/bash -c "source /ros_entrypoint.sh && \
#    source /home/pepper/ros/setup.bash && \
#    source /catkin_ws/robotics_group_gazebo_plugins/devel/setup.bash && \
#    source /home/pepper/ros_catkin_ws/devel/setup.bash &&  \
#    roslaunch pepper_moveit_config moveit_planner.launch"


