#!/bin/bash

NAME_CONTAINER=simulator1

export GAZEBO_MASTER_IP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $NAME_CONTAINER)
export GAZEBO_MASTER_URI=$GAZEBO_MASTER_IP:11345
#echo "Ejecutando cliente..."
gzclient 2>/dev/null 1>/dev/null
