#!/bin/bash

#if [[ $1 == "" ]] ; then
#    echo "./run-client idcontainer"
#    echo "Introducir el identificador del contenedor."
#    exit 1
#fi

#NAME_CONTAINER=$1
NAME_CONTAINER="ros/full-nvidia:pepper"

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

nvidia-docker run -it \
        --volume=$XSOCK:$XSOCK:rw \
        --volume=$XAUTH:$XAUTH:rw \
        --env="XAUTHORITY=${XAUTH}" \
        --env="DISPLAY" \
	--env="QT_X11_NO_MITSHM=1" \
        --user="pepper" \
    $NAME_CONTAINER
    #'roslaunch pepper_gazebo_plugin pepper_gazebo_plugin_Y20.launch'
    #'roscore & rosrun rviz rviz -d /opt/ros/indigo/share/naoqi_driver/share/pepper.rviz'
    #'bin/bash'
    #'roscore & rosrun rviz rviz -d /opt/ros/indigo/share/naoqi_driver/share/pepper.rviz'
    #"roslaunch pepper_gazebo_plugin pepper_gazebo_plugin_Y20.launch"
